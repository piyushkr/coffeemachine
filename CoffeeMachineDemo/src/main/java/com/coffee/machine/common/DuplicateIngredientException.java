package com.coffee.machine.common;

public class DuplicateIngredientException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public DuplicateIngredientException(String name) {
		super(name);
	}

}
