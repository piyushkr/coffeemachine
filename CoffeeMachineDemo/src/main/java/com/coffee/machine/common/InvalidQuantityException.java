package com.coffee.machine.common;

public class InvalidQuantityException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public InvalidQuantityException(String ingredientname, int amount) {
		super("Amount for ingredient " + ingredientname + " = " + amount + " is invalid");
	}

}
