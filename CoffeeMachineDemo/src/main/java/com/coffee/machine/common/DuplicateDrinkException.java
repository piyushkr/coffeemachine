package com.coffee.machine.common;

public class DuplicateDrinkException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public DuplicateDrinkException(String drinkName) {
		super(drinkName);
	}

}
