package com.coffee.machine.common;

import com.coffee.machine.ingredient.Ingredient;

public class IngredientOutOfStockException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public IngredientOutOfStockException(Ingredient ingredient) {
		super(ingredient.getName());
	}

}
