package com.coffee.machine.common;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.stream.Collectors;

import com.coffee.machine.drink.Drink;
import com.coffee.machine.ingredient.IngredientSizing;
import com.coffee.machine.json.Recipe;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class IOHelper {

	private static String directory = "";

	private IOHelper() {
		System.err.println("This class should not be initialized");
	}

	public static String brewingSuccess(Drink drink) {
		String output = drink.getName() + " is prepared";
		System.out.println(output);
		return output;
	}

	public static String brewingUnsuccessfull(Drink drink, Collection<IngredientSizing> unavailableIngredients) {

		String unavailableItems = unavailableIngredients.stream().map(item -> item.getIngredient().getName())
				.collect(Collectors.joining(","));
		StringBuilder sb = new StringBuilder();
		sb.append(drink.getName() + " cannot be prepared because " + unavailableItems + " are not available");
		System.out.println(sb.toString());
		return sb.toString();
	}

	public static Recipe input(String filepath) {
		ObjectMapper objectMapper = new ObjectMapper();
		Recipe recipe = null;

		try {
			File file = new File(directory + filepath);
			recipe = objectMapper.readValue(file, Recipe.class);
		} catch (JsonProcessingException e) {
			System.err.println("JSON Processing Exception");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IO Exception while reading file");
			e.printStackTrace();
		}
		return recipe;
	}

	public static void setDirectory(String dir) {
		directory = dir;
	}
}
