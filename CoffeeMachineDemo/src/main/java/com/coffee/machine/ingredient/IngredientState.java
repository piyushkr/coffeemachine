package com.coffee.machine.ingredient;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.coffee.machine.common.InvalidQuantityException;

/**
 * This class maintains the state of the all the ingredients of the
 * CoffeeMachine. All updates to ingredients happens from this class. This class
 * allows restocking of ingredients
 * 
 * @author pkumarda
 *
 */
public class IngredientState {
	private Map<String, IngredientSizing> state;

	public IngredientState(Map<String, Integer> ingredientsQuantity) {
		state = new ConcurrentHashMap<>();
		for (Entry<String, Integer> entry : ingredientsQuantity.entrySet()) {
			if (entry.getValue() < 0)
				throw new InvalidQuantityException(entry.getKey(), entry.getValue());
			state.put(entry.getKey(), new IngredientSizing(entry.getKey(), entry.getValue()));
		}
	}

	/**
	 * Updates the state of al the ingredients of one drink.
	 * 
	 * @param usedIngredients
	 */
	public void updateState(Collection<IngredientSizing> usedIngredients) {
		for (IngredientSizing ingredient : usedIngredients) {
			String usedIngredient = ingredient.getIngredient().getName();
			state.get(usedIngredient).updateAmount(ingredient.getAmount());
		}
	}

	/**
	 * Checks availability of the one ingredient, if it is available or not.
	 * 
	 * @param ingerdientSizing
	 * @return
	 */
	public boolean isAvailable(IngredientSizing ingerdientSizing) {
		String requiredIng = ingerdientSizing.getIngredient().getName();
		if (!this.state.containsKey(requiredIng))
			return false;

		return (this.state.get(requiredIng).getAmount() >= ingerdientSizing.getAmount());

	}

	/**
	 * Add stock for one Ingredient
	 * 
	 * @param stocks
	 */
	public void addStock(String name, int amount) {
		if (amount < 0)
			throw new InvalidQuantityException(name, amount);
		if (state.containsKey(name))
			state.get(name).updateAmount(-1 * amount);
		else
			state.put(name, new IngredientSizing(name, amount));
	}

	/**
	 * Add stocks for multiple ingredients
	 * 
	 * @param stocks
	 */
	public void addStock(Map<String, Integer> stocks) {
		for (Entry<String, Integer> stock : stocks.entrySet()) {
			if (stock.getValue() < 0)
				throw new InvalidQuantityException(stock.getKey(), stock.getValue());
		}
		for (Entry<String, Integer> stock : stocks.entrySet()) {
			addStock(stock.getKey(), stock.getValue());
		}
	}
}
