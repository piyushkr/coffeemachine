package com.coffee.machine.ingredient;

/**
 * Maintains the size of each ingredients. 
 * This classes responsibility is to tell how much amount of ingredient amount is available and update if asked upon.
 */
import com.coffee.machine.common.IngredientOutOfStockException;

public class IngredientSizing implements Comparable<IngredientSizing> {

	private Ingredient ingredient;
	private int amount;

	public IngredientSizing(String name, int amount) {
		this.ingredient = new Ingredient(name);
		this.amount = amount;
	}

	public Ingredient getIngredient() {
		return ingredient;
	}

	public int getAmount() {
		return amount;
	}

	/**
	 * Public method to update the amount of the ingredient.
	 * 
	 * @param diffAmount
	 */
	public void updateAmount(int diffAmount) {
		if (this.amount - diffAmount < 0)
			throw new IngredientOutOfStockException(this.ingredient);
		else
			this.amount = this.amount - diffAmount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ingredient == null) ? 0 : ingredient.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IngredientSizing other = (IngredientSizing) obj;
		if (ingredient == null) {
			if (other.ingredient != null)
				return false;
		} else if (!ingredient.equals(other.ingredient))
			return false;
		return true;
	}

	@Override
	public int compareTo(IngredientSizing o) {
		return this.ingredient.compareTo(o.ingredient);
	}

	@Override
	public String toString() {
		return "IngredientSizing [ingredient=" + ingredient + ", amount=" + amount + "]";
	}

}
