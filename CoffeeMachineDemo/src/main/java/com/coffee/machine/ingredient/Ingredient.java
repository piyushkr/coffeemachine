package com.coffee.machine.ingredient;

/**
 * Base class for Ingredient. Ingredients are defined by their name as of now.
 * We are not allowing a feature to update name of the ingredients
 * 
 * @author pkumarda
 *
 */
public class Ingredient implements Comparable<Ingredient> {

	private final String name;

	public Ingredient(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ingredient other = (Ingredient) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int compareTo(Ingredient o) {
		return this.name.compareTo(o.name);
	}

	@Override
	public String toString() {
		return "Ingredient [name=" + name + "]";
	}

}
