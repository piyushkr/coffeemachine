package com.coffee.machine.json;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
/**
 * POJO for machine Object. Used only while transforming
 * JSON to POJO via Jackson
 * 
 * @author pkumarda
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "outlets", "total_items_quantity", "beverages" })
public class Machine {

	@JsonProperty("outlets")
	private Outlets outlets;
	@JsonProperty("total_items_quantity")
	private Map<String, Integer> totalItemsQuantity;
	@JsonProperty("beverages")
	private Map<String, Map<String, Integer>> beverages;

	@JsonProperty("outlets")
	public Outlets getOutlets() {
		return outlets;
	}

	@JsonProperty("outlets")
	public void setOutlets(Outlets outlets) {
		this.outlets = outlets;
	}

	@JsonAnyGetter
	public Map<String, Integer> getTotalItemsQuantity() {
		return totalItemsQuantity;
	}

	@JsonProperty("total_items_quantity")
	public void setTotalItemsQuantity(Map<String, Integer> totalItemsQuantity) {
		this.totalItemsQuantity = totalItemsQuantity;
	}

	@JsonAnyGetter
	public Map<String, Map<String, Integer>> getBeverages() {
		return beverages;
	}

	@JsonProperty("beverages")
	public void setBeverages(Map<String, Map<String, Integer>> beverages) {
		this.beverages = beverages;
	}

	@Override
	public String toString() {
		return "Machine [outlets=" + outlets + ", totalItemsQuantity=" + totalItemsQuantity + ", beverages=" + beverages
				+ "]";
	}
	

}
