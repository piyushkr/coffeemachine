package com.coffee.machine.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * POJO to contain number of outlets in the machine. Used only while transforming
 * JSON to POJO via Jackson
 * 
 * @author pkumarda
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "count_n" })
public class Outlets {

	@JsonProperty("count_n")
	private Integer countN;

	@JsonProperty("count_n")
	public Integer getCountN() {
		return countN;
	}

	@JsonProperty("count_n")
	public void setCountN(Integer countN) {
		this.countN = countN;
	}

}