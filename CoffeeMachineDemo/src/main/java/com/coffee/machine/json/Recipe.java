package com.coffee.machine.json;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Pojo to Tranform the JSON into Recipe class. This class acts as raw material
 * for CoffeeMachine.
 * 
 * @author pkumarda
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "machine" })
public class Recipe {

	@JsonProperty("machine")
	private Machine machine;

	@JsonProperty("machine")
	public Machine getMachine() {
		return machine;
	}

	@JsonProperty("machine")
	public void setMachine(Machine machine) {
		this.machine = machine;
	}

	@Override
	public String toString() {
		return "RecipeJson [machine=" + machine + "]";
	}
}
