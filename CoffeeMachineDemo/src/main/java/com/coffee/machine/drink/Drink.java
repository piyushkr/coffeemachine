package com.coffee.machine.drink;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import com.coffee.machine.ingredient.IngredientSizing;
import com.coffee.machine.ingredient.IngredientState;

/**
 * Drink class has information about its ingredients and recipes. Allows reading
 * interface for checking if this drink can be made and provides all unavailable
 * Ingredients for this drink given the state of the machines. Once Ingredients
 * are decided, we are not allowing it to modify at runtime. i.e Recipe of a
 * drink is not allowed to change
 * 
 * @author pkumarda
 *
 */
public class Drink implements Comparable<Drink> {

	private final String name;
	private final Collection<IngredientSizing> ingredientList;

	public Drink(String name, Collection<IngredientSizing> ingredients) {
		this.name = name;
		this.ingredientList = ingredients;
		Collections.unmodifiableCollection(ingredientList);
	}

	public String getName() {
		return name;
	}

	/**
	 * Provides whether this drink be made or not given the machine state.
	 * 
	 * @param ingredientState
	 * @return
	 */

	public boolean canBeMade(IngredientState ingredientState) {
		for (IngredientSizing ingredient : ingredientList) {
			if (!ingredientState.isAvailable(ingredient)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Provides list of all unavailable ingredients for this drink given the
	 * state of the machine
	 * 
	 * @param ingredientState
	 * @return
	 */
	public Collection<IngredientSizing> unavailableIngredints(IngredientState ingredientState) {
		Collection<IngredientSizing> unavailableIngredients = new HashSet<>();
		for (IngredientSizing ingredient : ingredientList) {
			if (!ingredientState.isAvailable(ingredient)) {
				unavailableIngredients.add(ingredient);
			}
		}
		return unavailableIngredients;

	}

	public Collection<IngredientSizing> getIngredientList() {
		return ingredientList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Drink other = (Drink) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int compareTo(Drink o) {
		return this.name.compareTo(o.getName());
	}

}
