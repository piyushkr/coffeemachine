package com.coffee.machine.drink;

import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.coffee.machine.common.DuplicateDrinkException;
import com.coffee.machine.common.DuplicateIngredientException;
import com.coffee.machine.ingredient.IngredientSizing;

/**
 * Utility class to build all the drinks given their recipies.
 * 
 * @author pkumarda
 *
 */
public class DrinkBuilder {

	private DrinkBuilder() {
		System.err.println("Static class should not be initialized");
	}

	public static Set<Drink> prepareDrinks(Map<String, Map<String, Integer>> drinkRecipies) {
		Set<Drink> drinks = new HashSet<>();
		for (Entry<String, Map<String, Integer>> drinkEntry : drinkRecipies.entrySet()) {

			Set<IngredientSizing> ingredientSizings = new HashSet<>();
			for (Entry<String, Integer> ingredientEntry : drinkEntry.getValue().entrySet()) {
				IngredientSizing newSizing = new IngredientSizing(ingredientEntry.getKey(), ingredientEntry.getValue());
				boolean isAdded = ingredientSizings.add(newSizing);
				if (!isAdded)
					throw new DuplicateIngredientException(ingredientEntry.getKey());
			}
			boolean isAdded = drinks.add(new Drink(drinkEntry.getKey(), ingredientSizings));
			if (!isAdded)
				throw new DuplicateDrinkException(drinkEntry.getKey());
		}
		return drinks;
	}

}
