package com.coffee.machine.drink;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.coffee.machine.common.IOHelper;
import com.coffee.machine.ingredient.IngredientState;
import com.coffee.machine.json.Recipe;

/**
 * This class has responsbility to take the recipe of beverages and runs the
 * Coffee machine. Available Interfaces for clients to interact are:
 * 1.Restocking the Coffee Machine 2. Starting the CoffeeMaker
 * 
 * @author pkumarda
 *
 */
public class CoffeeMachine {
	private IngredientState ingredientState;
	private int numOutlet;
	private Set<Drink> availableDrinks;

	public CoffeeMachine(Recipe recipe) {
		this.numOutlet = recipe.getMachine().getOutlets().getCountN();
		this.ingredientState = new IngredientState(recipe.getMachine().getTotalItemsQuantity());
		availableDrinks = DrinkBuilder.prepareDrinks(recipe.getMachine().getBeverages());
	}

	/**
	 * Utility Private method to make drink and invoke state change after making
	 * the drink
	 * 
	 * @param drink
	 * @return
	 */
	private String make(Drink drink) {
		this.ingredientState.updateState(drink.getIngredientList());
		return IOHelper.brewingSuccess(drink);
	}

	/**
	 * This method runs the coffee machine and returns a list stating which
	 * drinks were and made and which were not made 
	 * If number of outlets are
	 * less than available drinks - Drinks will be served ordered on the drinks
	 * comparator which is name in this implementation 
	 * Note - Here number of
	 * outlets can be more than number of drinks, in that case same drink will
	 * be brewed multiple times if it is available
	 * 
	 * @return Collection<String> Output of Coffee Machines.
	 */
	public Collection<String> startCoffeeMachine() {
		Collection<String> output = new ArrayList<>();
		int outletCount = 1;
		int successfullDrinks = 0;
		while (outletCount <= numOutlet) {
			int servedDrinks = outletCount;
			for (Drink drink : availableDrinks) {
				outletCount++;
				if (successfullDrinks < numOutlet && drink.canBeMade(ingredientState)) {
					output.add(make(drink));
					successfullDrinks++;
				} else if (!drink.canBeMade(ingredientState)) {
					output.add(IOHelper.brewingUnsuccessfull(drink, drink.unavailableIngredints(ingredientState)));
				}
			}
			if (servedDrinks == outletCount)
				break;
		}
		return output;
	}

	/**
	 * Interface to add single ingredient to the coffee machine
	 * 
	 * @param name
	 * @param amount
	 */
	public void restock(String name, int amount) {
		this.ingredientState.addStock(name, amount);
	}

	/**
	 * Interface to add multiple ingredients at one time to coffee machine
	 * 
	 * @param addedStocks
	 */
	public void restock(Map<String, Integer> addedStocks) {
		this.ingredientState.addStock(addedStocks);
	}
}
