package com.coffee.test;

import com.coffee.machine.common.IOHelper;
import com.coffee.machine.drink.CoffeeMachine;
import com.coffee.machine.json.Recipe;
/**
 * This is just used for local testing. DONOT USE.
 * @author pkumarda
 *
 */
public class BasicMain {
	public static void main(String[] args) {
		
		String dir = "";
		if(args.length == 1)
			dir = args[0];
		
		Recipe recipe = IOHelper.input(dir + "src/main/resources/SampleInput.json");
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		coffeeMachine.startCoffeeMachine();
		coffeeMachine.restock("hot_water", 800);
		coffeeMachine.restock("sugar_syrup", 800);
		coffeeMachine.restock("tea_leaves_syrup", 800);
		coffeeMachine.startCoffeeMachine();
		//System.out.println(recipe);

	}
}
