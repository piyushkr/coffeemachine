/**
 * 
 */
package com.coffee.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;

import org.junit.Test;

import com.coffee.machine.common.IOHelper;
import com.coffee.machine.drink.CoffeeMachine;
import com.coffee.machine.json.Recipe;

/**
 * Test cases for testing Coffee Machine functionlaities. Test case name
 * provides insight on the functionality of the test.
 * 
 * @author pkumarda
 *
 */
public class CoffeeMachineTests {

	@Test
	public void basicWorkingTest() {

		Recipe recipe = IOHelper.input("src/main/resources/SampleInput.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Collection<String> logs = coffeeMachine.startCoffeeMachine();
		assertNotEquals(logs.size(), 0);
	}

	@Test
	public void equalDrinksAndSlotsTest() {

		Recipe recipe = IOHelper.input("src/main/resources/AllItemsAvailable.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Collection<String> logs = coffeeMachine.startCoffeeMachine();
		assertEquals(logs.stream().filter(line -> line.contains(" is prepared")).count(), 4);
	}

	@Test
	public void allDrinksAvailableButLessSlotsTest() {

		Recipe recipe = IOHelper.input("src/main/resources/AllItemsAvailableLessSlots.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Collection<String> logs = coffeeMachine.startCoffeeMachine();
		assertEquals(logs.size(), 3);
		assertEquals(logs.stream().filter(line -> line.contains(" is prepared")).count(), 3);
	}

	@Test
	public void allDrinksAvailableButLessDrinksThanSlotsTest() {

		Recipe recipe = IOHelper.input("src/main/resources/AllItemsAvailableLessDrinks.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Collection<String> logs = coffeeMachine.startCoffeeMachine();
		assertNotEquals(logs.stream().filter(line -> line.contains(" is prepared")).count(), 3);
	}

	@Test
	public void fewAvailableDrinksTest() {

		Recipe recipe = IOHelper.input("src/main/resources/SampleInput.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Collection<String> logs = coffeeMachine.startCoffeeMachine();
		assertEquals(logs.stream().filter(line -> line.equals("black_tea is prepared")).count(), 1);
		logs = coffeeMachine.startCoffeeMachine();
		assertEquals("", logs.stream().filter(line -> line.equals("black_tea is prepared")).count(), 0);
	}

	@Test
	public void fewUnAvailableDrinksTest() {

		Recipe recipe = IOHelper.input("src/main/resources/SampleInput.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Collection<String> logs = coffeeMachine.startCoffeeMachine();
		assertEquals(logs.stream().filter(line -> line.contains("green_tea cannot be prepared")).count(), 1);
	}

	@Test
	public void allUnAvailableDrinksTest() {

		Recipe recipe = IOHelper.input("src/main/resources/AllUnavailableDrinks.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Collection<String> logs = coffeeMachine.startCoffeeMachine();
		assertEquals(logs.stream().filter(line -> line.contains("cannot be prepared")).count(), logs.size());
	}

	@Test
	public void noDrinksRecipeTest() {
		Recipe recipe = IOHelper.input("src/main/resources/NoDrinksRecipe.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Collection<String> logs = coffeeMachine.startCoffeeMachine();
		assertEquals(logs.size(), 0);
	}

	@Test
	public void noQuantityProvidedTest() {
		Recipe recipe = IOHelper.input("src/main/resources/NoQuantityProvided.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Collection<String> logs = coffeeMachine.startCoffeeMachine();
		assertEquals(logs.stream().filter(line -> line.contains("cannot be prepared")).count(), logs.size());
	}

	/**
	 * This test is not need because jackson takes cares of duplicate entries
	 * while de-serializing the input.
	 */
	// @Test(expected= DuplicateIngredientException.class)
	public void duplicateIngredientsTest() {
		Recipe recipe = IOHelper.input("src/main/resources/DuplicateIngredient.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		coffeeMachine.startCoffeeMachine();
	}

	/**
	 * This test is not need because jackson takes cares of duplicate entries
	 * while de-serializing the input.
	 */
	// @Test(expected= DuplicateDrinkException.class)
	public void duplicateDrinksTest() {
		Recipe recipe = IOHelper.input("src/main/resources/DuplicateDrinks.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		coffeeMachine.startCoffeeMachine();
	}
}
