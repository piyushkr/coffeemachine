package com.coffee.test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import com.coffee.machine.common.IOHelper;

/**
 * Test runner with main method.
 * 
 * @author pkumarda
 *
 */
public class TestRunner {
	public static void main(String[] args) {
		if (args.length == 1)
			IOHelper.setDirectory(args[0]);

		Result result = JUnitCore.runClasses(JunitTestSuite.class);

		for (Failure failure : result.getFailures()) {
			System.out.println(failure.toString());
		}

		System.out.println("\n \n All tests successfull ran: " + result.wasSuccessful());
	}
}
