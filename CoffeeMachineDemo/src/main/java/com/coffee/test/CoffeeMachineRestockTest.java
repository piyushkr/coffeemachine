/**
 * 
 */
package com.coffee.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.coffee.machine.common.IOHelper;
import com.coffee.machine.common.InvalidQuantityException;
import com.coffee.machine.drink.CoffeeMachine;
import com.coffee.machine.json.Recipe;

/**
 * Test cases for Restocking of Ingredients. Test cases name provide insight on
 * the functionality of the test case.
 * 
 * @author pkumarda
 *
 */
public class CoffeeMachineRestockTest {

	@Test
	public void basicWorkingRestockingTest() {

		Recipe recipe = IOHelper.input("src/main/resources/SampleRestockInput.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Collection<String> logs = coffeeMachine.startCoffeeMachine();
		coffeeMachine.restock("hot_water", 100);
		assertNotEquals(logs.size(), 0);
	}

	@Test
	public void restockSingleIngredientTest() {

		Recipe recipe = IOHelper.input("src/main/resources/SampleRestockInput.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Collection<String> logs = coffeeMachine.startCoffeeMachine();
		assertEquals(logs.stream().filter(line -> line.contains("hot_coffee cannot be prepared")).count(), 1);

		coffeeMachine.restock("hot_water", 100);
		logs = coffeeMachine.startCoffeeMachine();
		assertEquals(logs.stream().filter(line -> line.contains("hot_coffee cannot be prepared")).count(), 0);
		assertEquals(logs.stream().filter(line -> line.contains("hot_coffee is prepared")).count(), 1);
	}

	@Test
	public void restockMultipleIngredientsTest() {

		Recipe recipe = IOHelper.input("src/main/resources/SampleRestockInput.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Collection<String> logs = coffeeMachine.startCoffeeMachine();
		assertEquals(logs.stream().filter(line -> line.contains("hot_coffee cannot be prepared")).count(), 1);

		coffeeMachine.restock("hot_water", 1000);
		coffeeMachine.restock("sugar_syrup", 100);
		coffeeMachine.restock("tea_leaves_syrup", 100);

		logs = coffeeMachine.startCoffeeMachine();
		assertEquals(logs.stream().filter(line -> line.contains("hot_coffee is prepared")).count(), 1);
		assertEquals(logs.stream().filter(line -> line.contains("hot_tea is prepared")).count(), 1);
	}

	@Test
	public void restockMultipleIngredientsTogetherTest() {

		Recipe recipe = IOHelper.input("src/main/resources/SampleRestockInput.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Collection<String> logs = coffeeMachine.startCoffeeMachine();
		assertEquals(logs.stream().filter(line -> line.contains("hot_coffee cannot be prepared")).count(), 1);
		Map<String, Integer> stocks = new HashMap<>();
		stocks.put("hot_water", 1000);
		stocks.put("sugar_syrup", 100);
		stocks.put("tea_leaves_syrup", 100);

		coffeeMachine.restock(stocks);
		logs = coffeeMachine.startCoffeeMachine();
		assertEquals(logs.stream().filter(line -> line.contains("hot_coffee is prepared")).count(), 1);
		assertEquals(logs.stream().filter(line -> line.contains("hot_tea is prepared")).count(), 1);
	}

	@Test(expected = InvalidQuantityException.class)
	public void restockSingleInvalidQuantityTest() {

		Recipe recipe = IOHelper.input("src/main/resources/SampleRestockInput.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		coffeeMachine.restock("hot_water", -10);
	}

	@Test(expected = InvalidQuantityException.class)
	public void restockBulkInvalidQuantityTest() {

		Recipe recipe = IOHelper.input("src/main/resources/SampleRestockInput.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Map<String, Integer> stocks = new HashMap<>();
		stocks.put("hot_water", -1000);
		stocks.put("sugar_syrup", 100);
		coffeeMachine.restock(stocks);
	}

	@Test
	public void restockMutilpleTimesTest() {

		Recipe recipe = IOHelper.input("src/main/resources/SampleRestockInput.json");
		assertNotNull("Recipe should not be null", recipe);
		CoffeeMachine coffeeMachine = new CoffeeMachine(recipe);
		assertNotNull("Coffee Machine cannot be null", coffeeMachine);
		Collection<String> logs = coffeeMachine.startCoffeeMachine();
		assertEquals(logs.stream().filter(line -> line.contains("hot_coffee cannot be prepared")).count(), 1);

		coffeeMachine.restock("hot_water", 100);
		logs = coffeeMachine.startCoffeeMachine();
		coffeeMachine.restock("sugar_syrup", 100);
		coffeeMachine.restock("tea_leaves_syrup", 100);
		logs = coffeeMachine.startCoffeeMachine();
		coffeeMachine.restock("hot_water", 100);
		assertEquals(logs.stream().filter(line -> line.contains("hot_coffee cannot be prepared")).count(), 1);
		assertEquals(logs.stream().filter(line -> line.contains("hot_coffee is prepared")).count(), 0);
	}
}
