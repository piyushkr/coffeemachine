package com.coffee.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test Suite which has two classes as of now.
 * 
 * @author pkumarda
 *
 */
@RunWith(Suite.class)

@Suite.SuiteClasses({ CoffeeMachineTests.class, CoffeeMachineRestockTest.class })

public class JunitTestSuite {
}