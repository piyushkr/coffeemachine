How to run this project

Option 1:
Using IDE
1. Import the Project in IDE.
2. Add library and add all the jars from folder - CoffeeMachineDemo/src/main/resources/libs/
3. Run the TestRunner class Main Program which is present in package "com.coffee.test" and it will execute all the test cases.


Option 2
Using Command-line

1. Download the project and move to folder CoffeeMachineDemo/src/main/java
2. Run following command to compile the java classes
	javac $(find . -name "*.java") -cp "../resources/libs/*" 
	
3. To execute all the test:
	java  -cp "../resources/libs/*:." com.coffee.test.TestRunner /Users/pkumarda/Downloads/personal/coding-scripts/CoffeeMachineDemo/

	Here last argument is the absolute directory of the project. 
	Example:
	If the project is downloaded at absolute path : /abc/def/
	Then Provide path as -  /abc/def/CoffeeMachineDemo/
	
Note:
1. All the jars are present in resources/lib folder
2. All the test jsons are present in resources folder.
3. There are total of two test classes and 16 test cases.